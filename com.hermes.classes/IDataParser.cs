using System;

namespace com.hermes.classes
{

	public delegate void DataParserEvent(object sender,IHermesDataStrategyResult argument);

	public interface IDataParser {
		event DataParserEvent Complete;
		void Parse(HermesDataItem item);
	}
	
}
