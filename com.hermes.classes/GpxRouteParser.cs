using System;

namespace com.hermes.classes
{
	/// <summary>
	/// Парсер данных о маршрутах полученных из Gpx файлов
	/// </summary>
	public sealed class GpxRouteParser : IHermesDataStrategy<IDataParser, IGpxRouteConfig, ITrackerData>
	{
		/// <summary>
		/// Стратегия обработки данных о маршрутах автотраспорта полученная в виде gpx файлов
		/// </summary>
		/// <param name="context">Контекст парсера.</param>
		/// <param name="config">Конфигуратор парсера.</param>
		/// <param name="data">Данные для обработки.</param>
		public IHermesDataStrategyResult Strategy(IDataParser context, IGpxRouteConfig config, ITrackerData data)
		{
			if (context == null)
				throw new ArgumentNullException(nameof(context));
			if (config == null)
				throw new ArgumentNullException(nameof(config));
			if (data == null)
				throw new ArgumentNullException(nameof(data));
			IHermesDataStrategyResult result = null;
			context.Complete += (sender, argument) =>
			{
				result = argument;
			};
			context.Parse((HermesDataItem)data);
			return result;
		}
	}




	
}
