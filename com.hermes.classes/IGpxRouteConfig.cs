namespace com.hermes.classes
{
	/// <summary>
	/// Gpx files config.
	/// </summary>
	public interface IGpxRouteConfig { 
		/// <summary>
		/// Gets or sets the minimum speed.
		/// </summary>
		/// <value>The minimum speed.</value>
		double MinSpeed { get; set; }
		/// <summary>
		/// Gets or sets the angle.
		/// </summary>
		/// <value>The angle.</value>
		double Angle { get; set; }
	}

	
}
