using System;

namespace com.hermes.classes
{
	/// <summary>
	/// Стратегия обработки данных о посещенных точках доставки
	/// </summary>
	public sealed class GpxViewPointsParser : IHermesDataStrategy<IDataParser, IGpxViewPointsConfig, ITrackerData>
	{

		/// <summary>
		/// Стратегия обработки данных о посещенных точках доставки транспортным средством полученная в виде gpx файлов
		/// </summary>
		/// <param name="context">Контекст парсера.</param>
		/// <param name="config">Конфигуратор парсера.</param>
		/// <param name="data">Данные для обработки.</param>
		public IHermesDataStrategyResult Strategy(IDataParser context, IGpxViewPointsConfig config, ITrackerData data)
		{
			if (context == null)
				throw new ArgumentNullException(nameof(context));
			if (config == null)
				throw new ArgumentNullException(nameof(config));
			if (data == null)
				throw new ArgumentNullException(nameof(data));
			IHermesDataStrategyResult result = null;
			context.Complete += (sender, argument) => {
				result = argument;
			};
			context.Parse((HermesDataItem)data);
			return result;
		}
	}

}
