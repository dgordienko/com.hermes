using System;

namespace com.hermes.classes
{
	/// <summary>
	/// Gpx view points config.
	/// </summary>
	public interface IGpxViewPointsConfig { 
		/// <summary>
		/// Gets or sets the search radius.
		/// </summary>
		/// <value>The search radius.</value>
		double SearchRadius { get; set; }
		/// <summary>
		/// Gets or sets the time.
		/// </summary>
		/// <value>The time.</value>
		TimeSpan Time { get; set; }
	}

}
