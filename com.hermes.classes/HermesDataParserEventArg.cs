using System;

namespace com.hermes.classes
{

	public sealed class HermesDataParserEventArg:EventArgs
	{ 
		public object Result { get; set; }
		public Exception Exception { get; set; }
	}
	public delegate void HermesDataParserEvent(object sender, HermesDataParserEventArg arg);
	
}
