namespace com.hermes.classes
{

	/// <summary>
	/// Gpx files data.
	/// </summary>
	public interface ITrackerData { 
		/// <summary>
		/// Gets or sets the header.
		/// </summary>
		/// <value>The header.</value>
		object Header { get; set; }
		/// <summary>
		/// Gets or sets the body.
		/// </summary>
		/// <value>The body.</value>
		object Body { get; set; }
	}
}
