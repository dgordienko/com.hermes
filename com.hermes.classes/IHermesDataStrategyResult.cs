using System;
namespace com.hermes.classes
{
	/// <summary>
	/// Hermes data strategy result.
	/// </summary>
	public interface IHermesDataStrategyResult {
		/// <summary>
		/// Gets or sets the result.
		/// </summary>
		/// <value>The result.</value>
		object Result { get; set; }
		/// <summary>
		/// Gets or sets the exception.
		/// </summary>
		/// <value>The exception.</value>
		Exception Exception { get; set; }
	}
	
}
