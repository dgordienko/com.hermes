﻿namespace com.hermes.classes
{
	
	/// <summary>
	/// Стратегия обработки данных о маршрутах автотраспорта
	/// </summary>
	public interface IHermesDataStrategy<in T,in C,in D>
	{
		/// <summary>
		/// Strategy the specified context, config and data.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="config">Config.</param>
		/// <param name="data">Data.</param>
		IHermesDataStrategyResult Strategy(T context, C config, D data);
	}

}

